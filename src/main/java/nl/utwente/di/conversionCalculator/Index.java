package nl.utwente.di.conversionCalculator;

import java.io.*;
import jakarta.servlet.*;
import jakarta.servlet.http.*;

/** Example of a Servlet that gets a Temperature in Celsius
 * and returns the matching temperature in Fahrenheit
 */

public class Index extends HttpServlet {
 	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private conversionCalculator cc;
	
    public void init() throws ServletException {
    	cc = new conversionCalculator();
    }	
	
  public void doGet(HttpServletRequest request,
                    HttpServletResponse response)
      throws ServletException, IOException {

    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Conversion Calculator";
    
    // Done with string concatenation only for the demo
    // Not expected to be done like this in the project
    out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE>" + title + "</TITLE>" +
                "<LINK REL=STYLESHEET HREF=\"styles.css\">" +
                		"</HEAD>\n" +
                "<BODY BGCOLOR=\"#FDF5E6\">\n" +
                "<H1>" + title + "</H1>\n" +              
                "  <P> Temperature in Celsius: " +
                   request.getParameter("degrees") + "\n" +
                "  <P>Temperature in Fahrenheit: " +
                   cc.getConversion(request.getParameter("degrees")) +
                "</BODY></HTML>");
  }
}
