package nl.utwente.di.conversionCalculator;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class TestConverter {

    @Test
    public void testConversion() {
        conversionCalculator converter = new conversionCalculator();
        double temp = converter.getConversion("0");
        Assertions.assertEquals(32.0, temp, 0.5, "Temp in Fahrenheit");
    }
}
